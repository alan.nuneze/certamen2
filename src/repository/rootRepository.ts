import rootInterface from './root.model.interface'

export const getRootMessage = () => {
    return buildMessage()
}

function buildMessage (): rootInterface {
    return {
        code: 200,
        message: 'HELLO WORLD!'
    }
}